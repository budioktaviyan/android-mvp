package id.kotlin.mvp.features.home

class HomePresenter(private val view: HomeView) {

    fun showData() {
        val models: List<HomeModel> = listOf(
                HomeModel("Title satu", "Deskripsi Satu"),
                HomeModel(title = "Title dua", desc = "Deskripsi Dua"),
                HomeModel(desc = "Deskripsi Tiga", title = "Title tiga")
        )
        view.onShowData(models)
    }
}