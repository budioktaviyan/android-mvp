package id.kotlin.mvp.features.home

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.kotlin.mvp.R
import id.kotlin.mvp.features.detail.DetailActivity
import kotlinx.android.synthetic.main.activity_main.*

class HomeActivity : AppCompatActivity(), HomeView, HomeListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val presenter = HomePresenter(this)
        presenter.showData()
    }

    override fun onShowData(models: List<HomeModel>) {
        rv_main.adapter = HomeAdapter(models, this)
    }

    override fun onItemClick(model: HomeModel) {
        startActivity(Intent(this, DetailActivity::class.java).apply {
            putExtra("MODEL", model)
        })
    }
}