package id.kotlin.mvp.features.home

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HomeModel(val title: String, val desc: String) : Parcelable