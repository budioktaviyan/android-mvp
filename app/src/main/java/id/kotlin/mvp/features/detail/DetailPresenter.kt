package id.kotlin.mvp.features.detail

import id.kotlin.mvp.features.home.HomeModel

class DetailPresenter(private val view: DetailView) {

    fun showData(model: HomeModel?) {
        model?.let { view.onShowData(it) }
    }
}