package id.kotlin.mvp.features.detail

import id.kotlin.mvp.features.home.HomeModel

interface DetailView {

    fun onShowData(model: HomeModel)
}