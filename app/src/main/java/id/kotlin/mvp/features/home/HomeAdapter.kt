package id.kotlin.mvp.features.home

import android.support.v7.widget.RecyclerView.Adapter
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.kotlin.mvp.R
import kotlinx.android.synthetic.main.item_main.view.*

class HomeAdapter(private val models: List<HomeModel>,
                  private val listener: HomeListener) : Adapter<HomeHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHolder =
            HomeHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_main, parent, false), listener)

    override fun onBindViewHolder(holder: HomeHolder, position: Int) {
        holder.bind(models[holder.adapterPosition])
    }

    override fun getItemCount(): Int = models.size
}

class HomeHolder(itemView: View,
                 private val listener: HomeListener) : ViewHolder(itemView) {

    fun bind(model: HomeModel) {
        with(itemView) {
            tv_title.text = model.title
            tv_desc.text = model.desc
            rootView.setOnClickListener { listener.onItemClick(model) }
        }
    }
}

interface HomeListener {

    fun onItemClick(model: HomeModel)
}