package id.kotlin.mvp.features.home

interface HomeView {

    fun onShowData(models: List<HomeModel>)
}