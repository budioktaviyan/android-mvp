package id.kotlin.mvp.features.detail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import id.kotlin.mvp.R
import id.kotlin.mvp.features.home.HomeModel
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity(), DetailView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val model = intent?.extras?.getParcelable<HomeModel>("MODEL")
        val presenter = DetailPresenter(this)
        presenter.showData(model)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onShowData(model: HomeModel) {
        title = model.title
        tv_detail_desc.text = model.desc
    }
}